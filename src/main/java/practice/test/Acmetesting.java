package practice.test;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Acmetesting {

	ChromeDriver driver;
	
	@Test
	public void acmeTesting() {
		System.setProperty("webdriver.chrome.driver","C://TestLeaf//PatternObjectModel//drivers//chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com/");
		driver.findElementByXPath("//input[@id='email']").sendKeys("nirmal930118@gmail.com");
		driver.findElementByXPath("//input[@id='password']").sendKeys("Welcome@2");
		driver.findElementByXPath("//button[text()='Log In']").click();
		Actions sc = new Actions(driver);
		WebElement eleVendors = driver.findElementByXPath("(//button[@class='btn btn-default btn-lg'])[4]");
		sc.moveToElement(eleVendors).clickAndHold().perform();
		WebElement eleSearchVendors = driver.findElementByXPath("//a[text()='Search for Vendor']");
		sc.moveToElement(eleSearchVendors).click().perform();
		driver.findElementByXPath("(//input[@class='form-control'])[1]").sendKeys("DE763212");
		driver.findElementByXPath("//button[@id='buttonSearch']").click();
		String VendorName = driver.findElementByXPath("(//table[@class='table']//td)[1]").getText();
		System.out.println(VendorName);
		driver.close();
		
	}
}
